﻿using UnityEngine;

namespace Assets.Skrypty
{
    //w tej klasie chodzi o to, jak dotkniemy monety to ona ogarnia że coś ją dotkneło i zmika i sie respi gdzieś indziej jako klon
    public class CoinPickup : MonoBehaviour
    {
        public int PunktyZaZebranieCoina;

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<Poruszanie>() == null)
                return;

            
            ScoreManager.DodajPunkt(PunktyZaZebranieCoina);
            GameManager.randomRespawnOneCoin(gameObject);
            Destroy(gameObject);
        }
	
    }
}
