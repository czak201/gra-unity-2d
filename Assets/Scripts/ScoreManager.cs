﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Skrypty
{
    public class ScoreManager : MonoBehaviour
    {
        public static int punktyGracza;
        Text PointsNumber;

        public void Start ()
        {
            PointsNumber = GetComponent<Text>();
            punktyGracza = 0;
        }

        public void Update()
        {
            PointsNumber.text = "" + punktyGracza;
        }
	

        public static void DodajPunkt(int punktDoDodania)
        {
            punktyGracza += punktDoDodania;
        }

        public static void OdejmijPunkt(int punktDoOdjecia)
        {
            punktyGracza -= punktDoOdjecia;
        }
    }
}
