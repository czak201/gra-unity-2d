﻿using UnityEngine;
using UnityEngine.UI;

public class MenuPause : MonoBehaviour {

    public Canvas pauseMenu;
    public Button resumeButton;
    public Button exitButton;
    public Rigidbody2D meniu;

    // Use this for initialization
    void Start () {
        pauseMenu = pauseMenu.GetComponent<Canvas>();
        resumeButton = resumeButton.GetComponent<Button>();
        exitButton = exitButton.GetComponent<Button>();
        pauseMenu.enabled = false;
    }
	
	void Update () {
        //po wciśnięciu ESC ma wyskoczyć pauza
        if (Input.GetKey(KeyCode.Escape))
        {
            meniu.position = new Vector3(Screen.width/2, Screen.height / 2);
            pauseMenu.enabled = true;
        }
    }

    //funkcja wyjscie z gry
    public void ExitGame()
    {
        Application.Quit();
    }

    //funkcja wylaczenia okna pauzy i kontynuacji gry
    public void ResumeGame()
    {
        meniu.position = new Vector3(Screen.width, Screen.height * 2);
        pauseMenu.enabled = false;
    }
}
