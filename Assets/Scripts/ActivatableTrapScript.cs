﻿using UnityEngine;
using System.Collections;

public class ActivatableTrapScript : MonoBehaviour {
	Animator animator;
	Collider2D trigger;
	public int Damage;
	
	void Start()
	{
		animator = GetComponent<Animator>();
		trigger = GetComponent<Collider2D>();
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.isTrigger != true && other.CompareTag("Enemy"))
		{
			animator.SetBool("Activated",true);
			other.SendMessageUpwards("ObrazeniaDlaZombika", Damage);
		}
		if (other.isTrigger != true && other.CompareTag("Player"))
		{
			animator.SetBool("Activated",true);
			other.SendMessageUpwards("DamageDealtToPlayer", Damage);
			
		}
	}
	public void Destroy()
	{
		Destroy (gameObject);
	}
}
