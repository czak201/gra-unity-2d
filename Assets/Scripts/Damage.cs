﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour {

	int dmg;
	Collider2D trigger;
	NPCManager parentclass;

	// Use this for initialization
	void Start () {
		parentclass = transform.parent.GetComponent<NPCManager>();
		dmg = parentclass.MeleeDamage;
		trigger = GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.isTrigger != true && other.CompareTag("Enemy"))
		{
			other.SendMessageUpwards("ObrazeniaDlaZombika", dmg);
			if(trigger.isActiveAndEnabled)
				trigger.enabled = false;
		}
		if (other.isTrigger != true && other.CompareTag("Player"))
		{
			if(trigger.enabled == true)
				trigger.enabled = false;
			other.SendMessageUpwards("DamageDealtToPlayer", dmg);

		}
	}
}
