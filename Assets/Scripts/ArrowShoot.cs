﻿using UnityEngine;
using System.Collections;

public class ArrowShoot : MonoBehaviour
{
	public int Speed;
	public int manaCost;
	public GameObject arrow;
	private Animator animator;
	private int x,y,r;
	void Start()
	{
		animator = gameObject.GetComponent<Animator> ();

	}
	public void Shoot()
	{
		if (PlayerManager.CurrentMana - manaCost >= 0) {
			x = 0;
			y = 0;
			r = 0;
			Vector2 pozycja = gameObject.GetComponentInParent<Rigidbody2D> ().transform.position;

			if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_right")) {
				pozycja.x = pozycja.x + 3;
				x = Speed;
				r = 0;
			}
			if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_left")) {
				pozycja.x = pozycja.x - 3;
				x = -Speed;
				r = 180;
			}
			if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_up")) {
				pozycja.y = pozycja.y + 3;
				y = Speed;
				r = 90;
			}
			if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_down")) {
				pozycja.y = pozycja.y - 3;
				y = -Speed;
				r = 270;
			}
			GameObject goneArrow = (GameObject)Instantiate (arrow, pozycja, Quaternion.Euler (0, 0, r));
			goneArrow.GetComponent<Rigidbody2D> ().velocity = new Vector2 (x, y);
			PlayerManager.CurrentMana -= manaCost;
		}
	}
}
