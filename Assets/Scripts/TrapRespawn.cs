﻿using UnityEngine;
using System.Collections;

public class TrapRespawn : MonoBehaviour {

    public GameObject[] orcToRandomSpawn;
	float x,y;
	void Start()
	{
		x=gameObject.transform.position.x;
		y=gameObject.transform.position.y;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {
			if(other.gameObject.transform.position.x>gameObject.transform.position.x)
			{
				x -= 3;
			}else
				x += 3;
			
			if(other.gameObject.transform.position.y>gameObject.transform.position.y)
			{
				y -= 3;
			}else
				y += 3;
			
			Vector2 randomPositionOrc = new Vector2(x, y);

            for (int i = 0; i < Random.Range(2, 5);i++)
            {
                Instantiate(orcToRandomSpawn[Random.Range(0, orcToRandomSpawn.Length)], randomPositionOrc, Quaternion.identity);
            }

			Destroy (gameObject);
        }
    }
}
