﻿using UnityEngine;
using System.Collections;

public class DamagingParticleTrigger : MonoBehaviour {
	public int dmg;
	Collider2D arrowTrigger;
	// Use this for initialization
	void Start () {
		arrowTrigger = gameObject.GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Enemy")||other.CompareTag("Player")||other.CompareTag("Wall"))
		{
			if(other.CompareTag("Player"))
				other.SendMessageUpwards("DamageDealtToPlayer", dmg);
			if(other.CompareTag("Enemy"))
				other.SendMessageUpwards("ObrazeniaDlaZombika", dmg);

			Destroy(gameObject);
		}
	}
}
