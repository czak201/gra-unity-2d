﻿using System;
using UnityEngine;
using UnityEngine.UI;

//klasa używana w menu głównym

public class MainMenu : MonoBehaviour {

    //dwa canvasy w których jest okienko wyjścia i opcji
    public Canvas exitMenuCanvas;
    public Canvas optionsMenuCanvas;

    public Button startButton;
    public Button exitButton;
    public Button optionsButton;

    //panele wyjścia i opcji zeby mozna było im położenie zmieniać
    public GameObject exitMenu;
    public GameObject optionsMenu;
    public GameObject infoText;

    private GameObject currentKey;

    //potrzebne zeby wyswietlac tekst i pobierać indeks z suwaka
    public Text resText;
    public Slider sliderResolution;

    //tekst do wyświetlania klawiszy sterowania które zmieniamy
    public Text upText, downText, leftText, rightText, attackText;

    void Start () {
        infoText.SetActive(false); //okienko "wcisnij klawisz"
        exitMenuCanvas.enabled = false; //okienko "czy chcesz wyjsc"
        optionsMenuCanvas.enabled = false; //okienko opcji

        //Przypisanie defaultowych klawiszy po wlaczeniu gry
        Poruszanie.keys.Add("Up", KeyCode.W);
        Poruszanie.keys.Add("Down", KeyCode.S);
        Poruszanie.keys.Add("Left", KeyCode.A);
        Poruszanie.keys.Add("Right", KeyCode.D);
        Poruszanie.keys.Add("Attack", KeyCode.Space);
        Poruszanie.keys.Add("Spell1", KeyCode.Alpha1);
        Poruszanie.keys.Add("Spell2", KeyCode.Alpha2);
        Poruszanie.keys.Add("Spell3", KeyCode.Alpha3);
        Poruszanie.keys.Add("Use", KeyCode.F);
        Poruszanie.keys.Add("Potion HP", KeyCode.Alpha4);
        Poruszanie.keys.Add("Potion MP", KeyCode.Alpha5);

        //Wyświetlanie aktualnych klawiszy w ustawieniach
        upText.text = Poruszanie.keys["Up"].ToString();
        downText.text = Poruszanie.keys["Down"].ToString();
        leftText.text = Poruszanie.keys["Left"].ToString();
        rightText.text = Poruszanie.keys["Right"].ToString();
        attackText.text = Poruszanie.keys["Attack"].ToString();
    }
    
    //wlaczenie levela 1
    public void StartLevel()
    {
        Application.LoadLevel(2);
    }

    //wyjście z gry
    public void ExitGame()
    {
        Application.Quit();
    }

    //funkcja wciśnięcia exit
    public void ExitPress()
    {
        exitMenu.transform.position = new Vector3(Screen.width/2, Screen.height/2);
        exitMenuCanvas.enabled = true;
        startButton.enabled = false;
        exitButton.enabled = false;
        optionsButton.enabled = false;
    }

    //funkcja wyłączenia okienka z pytaniem czy wyjść z gry
    public void CloseQuitMenu()
    {
        exitMenu.transform.position = new Vector3(Screen.width, Screen.height * 2);
        exitMenuCanvas.enabled = false;
        startButton.enabled = true;
        exitButton.enabled = true;
        optionsButton.enabled = true;
    }

    //funkcja do pokazania okienka z opcjami
    public void ShowOptions()
    {
        optionsMenu.transform.position = new Vector3(Screen.width / 2, Screen.height / 2);
        resText.text = "" + Screen.width + "x" + Screen.height;
        optionsMenuCanvas.enabled = true;
        startButton.enabled = false;
        exitButton.enabled = false;
        optionsButton.enabled = false;
    }

    //funkcja do schowania okienka z opcjami
    public void HideOptions()
    {
        optionsMenu.transform.position = new Vector3(Screen.width, Screen.height * 2);
        optionsMenuCanvas.enabled = false;
        startButton.enabled = true;
        exitButton.enabled = true;
        optionsButton.enabled = true;
    }

    //funkcja do zatwierdzenia ustawień w opcjach
    public void ApplyChanges()
    {
        if(sliderResolution.value==0)
        {
            resText.text = "800x600";
            Screen.SetResolution(800, 600, false);
        }
        if (sliderResolution.value == 1)
        {
            resText.text = "1024x768";
            Screen.SetResolution(1024, 768, false);
        }
        if (sliderResolution.value == 2)
        {
            resText.text = "1280x600";
            Screen.SetResolution(1280, 600, false);
        }
        optionsMenuCanvas.enabled = false;
        optionsMenuCanvas.transform.position = new Vector3(Screen.width, Screen.height * 2);
        startButton.enabled = true;
        exitButton.enabled = true;
        optionsButton.enabled = true;
    }

    //funkcja do anulowania ustawień w opcjach
    public void CancelChanges()
    {
        optionsMenu.transform.position = new Vector3(Screen.width, Screen.height * 2);
        optionsMenuCanvas.enabled = false;
        startButton.enabled = true;
        exitButton.enabled = true;
        optionsButton.enabled = true;
    }

    //funkcja do pokazywania rozdzielczości po ruszaniu suwakiem
    public void ResChange()
    {
        if (sliderResolution.value == 0)
        {
            resText.text = "800x600";
        }
        if (sliderResolution.value == 1)
        {
            resText.text = "1024x768";
        }
        if (sliderResolution.value == 2)
        {
            resText.text = "1280x600";
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        currentKey = clicked;
    }

    void OnGUI()
    {
        if (currentKey !=null)
        {
            infoText.SetActive(true);
            //jak wcisniemy jakis klawisz to odpala sie event z nazwą tego klawisza
            Event e = Event.current;
            //sprawdzamy czy event jest klawiszem
            if (e.isKey)
            {
                Poruszanie.keys[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;
                infoText.SetActive(false);
            }
        }
    }
}

