﻿using UnityEngine;
using System.Collections;

public class NPCMovement : MonoBehaviour {
	//speed odpowiada szybkości poruszania się NPC
    public float speed;
	//przypisane rigidboty gracza i npc
    private Rigidbody2D playerBody;
    private Rigidbody2D enemyBody;
	//boolean sprawdzajacy czy gracz jest w zasiegu 'wzrodku'
    private bool player_in_sight = false;
	//animator npc
	public Animator enemyAnim;
    //zmienne do poruszania sie w osi X i osi Y
    float x, y;
	//booleany potrzebne do algorytmu poruszania
    bool left = false, right = false, up = false, down = false;

    void Start()
    {
        playerBody = GameObject.Find("Player").GetComponent<Rigidbody2D>();
		enemyBody = gameObject.GetComponentInParent<Rigidbody2D>();
    }

    //funkcja odpowiadająca za zauważenie gracza jak wejdzie w zasieg collidera
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            player_in_sight = true;
        }
    }

    //jak player wyjdzie z okręgu to ork staje
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            player_in_sight = false;
			//zatrzymywanie NPC
            x = 0;
            y = 0;
            enemyBody.velocity = new Vector2(x, y);
        }
    }
	
	void Update () {
        //wykrywanie pozycji gracza
        if (player_in_sight == true)
        {
            if (enemyBody.position.x < playerBody.position.x)
            {//jezeli gracz jest dalej od NPC w osi X 
                if (!right)
                    right = true;
            }else
				if (right)
					right = false;
            if (enemyBody.position.x > playerBody.position.x)
			{//jezeli gracz jest w tyle od NPC w osi X 
                if (!left)
                    left = true;
            }else
				if (left)
					left = false;
            if (enemyBody.position.y < playerBody.position.y)
            { //jezeli gracz jest wyzej od NPC
                if (!up)
                    up = true;
            }else
				if (up)
					up = false;
            if (enemyBody.position.y > playerBody.position.y)
            {//jezeli gracz jest nziej od NPC
                if (!down)
                    down = true;
            }else
				if (down)
					down = false;

            //jezeli ktorys z warunkow wyzej zmieni boolean na true przypisujemy odpowiednie wartosci do X i Y
            if (right)
                x = speed;
            if (left)
                x = -speed;
            if (up)
                y = speed;
            if (down)
                y = -speed;
			//jezeli ktoryz z warunkow zmieni boolean na false przypisujemy odpowiednie wartosci
            if (!right && !left)
                x = 0;
            if (!up && !down)
                y = 0;
            if (right && left)
                x = 0;
            if (up && down)
                y = 0;

            //włączanie animacji chodzenia
			if(!enemyAnim.GetBool("attack_up")&&
			   !enemyAnim.GetBool("attack_down")&&
			   !enemyAnim.GetBool("attack_left")&&
			   !enemyAnim.GetBool("attack_right"))
			{
				if ((playerBody.position.x + playerBody.position.y) > (enemyBody.position.x + enemyBody.position.y)) {
					if ((playerBody.position.x - enemyBody.position.x) > (playerBody.position.y - enemyBody.position.y)) {
							enemyAnim.SetBool("walk_right", true);

						enemyAnim.SetBool("walk_down", false);
						enemyAnim.SetBool("walk_left", false);
						enemyAnim.SetBool("walk_up", false);
						enemyAnim.SetBool("idle", false);
					} else {
						enemyAnim.SetBool("walk_up", true);
						
						enemyAnim.SetBool("walk_right", false);
						enemyAnim.SetBool("walk_down", false);
						enemyAnim.SetBool("walk_left", false); 	
						enemyAnim.SetBool("idle", false);	
					}
				} else {
					if ((playerBody.position.x - enemyBody.position.x) > (playerBody.position.y - enemyBody.position.y)) {
						enemyAnim.SetBool("walk_down", true);
						
						enemyAnim.SetBool("walk_right", false);
						enemyAnim.SetBool("walk_left", false);
						enemyAnim.SetBool("walk_up", false);
						enemyAnim.SetBool("idle", false);
					} else {
						enemyAnim.SetBool("walk_left", true);
						
						enemyAnim.SetBool("walk_right", false);
						enemyAnim.SetBool("walk_down", false);
						enemyAnim.SetBool("walk_up", false);
						enemyAnim.SetBool("idle", false);
					}
				}
			}
			//Nadawanie szybkosci NPC
			if(enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("attack_up")||enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("attack_down")||
			   enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("attack_left")||enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("attack_right")){
				enemyBody.velocity = new Vector2(0, 0);
			}else{
            	enemyBody.velocity = new Vector2(x, y);
			}

        }
        //wyłączanie animacji chodzenia
        else
        {
            if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_right"))
            {
                enemyAnim.SetBool("walk_right", false);
                enemyAnim.SetBool("idle", true);
            }
            if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_left"))
            {
                enemyAnim.SetBool("walk_left", false);
                enemyAnim.SetBool("idle", true);
            }
            if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_up"))
            {
                enemyAnim.SetBool("walk_up", false);
                enemyAnim.SetBool("idle", true);
            }
            if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_down"))
            {
                enemyAnim.SetBool("walk_down", false);
                enemyAnim.SetBool("idle", true);
            }
        }
    }
        
}
