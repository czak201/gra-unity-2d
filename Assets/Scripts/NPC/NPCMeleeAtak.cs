﻿
using UnityEngine;
using System.Collections;

public class NPCMeleeAtak : MonoBehaviour {
	//Rigidbody gracza i wroga 
	private Rigidbody2D player;
	private Rigidbody2D enemy;
	//Boolean sprawdzajacy czy gracz jest w zasiegu ataku wroga
	private bool player_in_range = false;
	//Triggery góra,dół,prawo,lewo odpowiadajace za zadawanie obrazen 
	public Collider2D attackTriggerUp;
	public Collider2D attackTriggerDown;
	public Collider2D attackTriggerRight;
	public Collider2D attackTriggerLeft;
	//Animator kierowanego wroga
	public Animator enemyAnim;
	void Start()
	{
		player = GameObject.Find("Player").GetComponent<Rigidbody2D>();
		enemy = gameObject.GetComponentInParent<Rigidbody2D>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{ //funkcja wykonywana po wejsciu innego collidera w collider npc
		if(other.gameObject.name == "Player")
		{//jezeli collider nalezy do obiektu o nazwie PLayer zmieniamy boolean na true.
			player_in_range = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{//funkcja wykonywana po wyjsciu collidera z zasiegu collidera NPC
		if (other.gameObject.name == "Player")
		{//jezeli collider nalezy do obiektu o nazwie PLayer zmieniamy boolean na false
			player_in_range = false;
		}
	}	
	// Update jest wykonywany raz co klatke
	void Update () {
		   if (player_in_range){
		   	//jezeli gracz jest w zasiegu ataku to zmieniamy wszystkie booleany w animatorze na false
			//a zostawiamy tylko ten boolean który odpowiada za atak w stronę w którą npc aktualnie się poruszał
			if (enemyAnim.GetBool ("walk_up") &&
			    enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_up")) {

				enemyAnim.SetBool ("attack_up", true);
				enemyAnim.SetBool("walk_up", false);

			}

			if (enemyAnim.GetBool ("walk_down") &&
			    enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_down")) {

				enemyAnim.SetBool ("attack_down", true);
				enemyAnim.SetBool("walk_down",false);

			}

			if (enemyAnim.GetBool ("walk_left") &&
			    enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_left")) {

				enemyAnim.SetBool ("attack_left", true);
				enemyAnim.SetBool("walk_left", false);		

			}

			if (enemyAnim.GetBool ("walk_right") &&
			    enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("walk_right")) {

				enemyAnim.SetBool ("attack_right", true);
				enemyAnim.SetBool("walk_right", false);

			}

		} else {
			if(enemyAnim.GetBool("attack_up"))
				enemyAnim.SetBool("attack_up",false);

			if(enemyAnim.GetBool("attack_down"))
				enemyAnim.SetBool("attack_down",false);

			if(enemyAnim.GetBool("attack_left"))
				enemyAnim.SetBool("attack_left",false);

			if(enemyAnim.GetBool("attack_right"))
				enemyAnim.SetBool("attack_right",false);

			attackTriggerUp.enabled=false;
			attackTriggerDown.enabled=false;
			attackTriggerLeft.enabled=false;
			attackTriggerRight.enabled=false;
		}

	}
}