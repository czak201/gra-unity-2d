﻿using UnityEngine;
using System.Collections;

public class NPCManager : MonoBehaviour {
    //jak zombi straci życie to zostanie zdezintegrowany
    public int NPCHealth;
	private int NPCCurrentHealth;
	private int healthBarLenght;
	public int MeleeDamage;
    public GameObject[] itemsToDrop;
    public float percentSpawn;
	void Start()
	{
		NPCCurrentHealth=NPCHealth;
		healthBarLenght = (Screen.width / 6) * (NPCCurrentHealth /NPCHealth);
	}

    public void ObrazeniaDlaZombika(int dmg)
    {
       	NPCCurrentHealth -= dmg;
        if (NPCCurrentHealth < 1)
        {
            Destroy(gameObject);
            DropSomeItems();
        }

    }

    public void DropSomeItems()
    {
        if (Random.value <= percentSpawn)
        {
            Debug.Log("random" + Random.value);
            Instantiate(itemsToDrop[Random.Range(0, itemsToDrop.Length)], new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y), Quaternion.identity);
            Debug.Log("pozycja"+ new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.x));
        }
    }

	void OnGUI() {
		Vector2 screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
//		Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, gameObject.GetComponent<Rigidbody2D>().position);
		GUI.Box(new Rect(screenPoint.x-60, Screen.height-(screenPoint.y+50), healthBarLenght, 20), NPCCurrentHealth + "/" + NPCHealth);
	}
}
