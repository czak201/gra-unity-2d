﻿using Assets.Skrypty;
using UnityEngine;

public class EnemyTouch : MonoBehaviour
{
    // po dotknięciu "Player" ma odjąć punkty
    public int dmg;

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "Player")
        {
            ScoreManager.OdejmijPunkt(100);
        }
    }
}