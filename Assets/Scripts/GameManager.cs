﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    //zmienne potrzebne do zbierania monet
    [SerializeField]
    public GameObject[] monetyArray;
    public int maxMonet, minMonet;

    public Slider gameSpeedSlider; //suwak prędkości gry
    public Text speedValue;

    public GameObject orcToRandomSpawn;
	public GameObject skeletonToRandomSpawn;

    //zmienne potrzebne do blokowania kamery żeby daleko nie leciała
    /*public float xMax;
    public float xMin;
    public float yMax;
    public float yMin;*/
    private Transform target; //cel śledzenia kamery

	public static int ZycieGracza;
	public int Zycie;


    void Start()
    {
        randomRespawnCoins(monetyArray, maxMonet, minMonet); //wywolanie funkcji losowego tworzenia monet
        target = GameObject.Find("Player").transform; //rozpoczęcie śledzenia gracza kamerą
    }

    void Update()
    {
        //klawiszem R restartujemy poziom 1
        if (Input.GetKeyDown(KeyCode.R))
        {
            Application.LoadLevel(1);
        }
    }

    //LateUpdate is called every frame, if the Behaviour is enabled.
    void LateUpdate()
    {
        transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
    }

    //funkcja do randomowego spawnowania monet
    void randomRespawnCoins(GameObject[] monety, int max, int min)
    {
        int liczbaMonet = Random.Range(minMonet, maxMonet);
        for (int i = 0; i < liczbaMonet; i++)
        {
			Vector2 losowaPozycja = new Vector2(Random.Range(12,83), Random.Range(13,67));
            GameObject wyborMonety = monetyArray[Random.Range(0, monetyArray.Length)];
            Instantiate(wyborMonety, losowaPozycja, Quaternion.identity);
        }
    }

    //funkcja do randomowego spawnienia jednej monety
    public static void randomRespawnOneCoin(GameObject moneta)
    {
        Vector2 losowaPozycja = new Vector2(Random.Range(12, 83), Random.Range(13, 67));
        Instantiate(moneta, losowaPozycja, Quaternion.identity);
    }

    //funkcja do restartowania levela1 przyciskiem
    public void resetGame()
    {
        Application.LoadLevel(3);
    }

    //funkcja powrotu do menu głównego
    public void backToMainMenu()
    {
        Application.LoadLevel(0);
    }

    public void SetGameSpeed()
    {
        Time.timeScale = gameSpeedSlider.value;
        speedValue.text = gameSpeedSlider.value.ToString();
    }

    public void resetGameSpeed()
    {
        Time.timeScale = 1;
        speedValue.text = "1";
    }

    public void spawnOrcRandom()
    {
        Vector2 randomPositionOrc = new Vector2(Random.Range(target.transform.position.x-5 , target.transform.position.x + 5), Random.Range(target.transform.position.y - 5, target.transform.position.y + 5));
        Instantiate(orcToRandomSpawn, randomPositionOrc, Quaternion.identity);
    }
}


