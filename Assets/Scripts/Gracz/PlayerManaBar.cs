﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerManaBar : MonoBehaviour {

	private Image fillBar;
	// Use this for initialization
	void Start () {
		fillBar = this.GetComponent<Image>();
	}

	// Update is called once per frame
	void Update () {
		fillBar.fillAmount = (float) PlayerManager.CurrentMana/PlayerManager.MaxMana;

	}
}
