﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Poruszanie : MonoBehaviour {
    public float speed;
    //public static KeyCode goUp, goLeft, goDown, goRight;
	public Animator playerAnim;

	float x, y;
	bool left=false, right=false, up=false, down=false;

    //Dictionary jest to coś jak List<> tylko inne i jest w bibliotece Collections, pierwa wartość to tak jak by to co potrzebujemy a druga to wartość. 
    //W tym przypadku bedzie to keys["Up"] co program ogarnie ze chcemy wartość o nazwie "Up" która ma wartość KeyCode.W (do góry). Ta wiem, źle tłumacze, wygoogluj se
    //Przyda sie później do statystyk, tak myśle     Dictionary<TKey, TValue>
    public static Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();

    void Start()
    {
        //przed buildem zakomentować
        keys.Add("Up", KeyCode.W);
        keys.Add("Down", KeyCode.S);
        keys.Add("Left", KeyCode.A);
        keys.Add("Right", KeyCode.D);
        keys.Add("Attack", KeyCode.Space);
		keys.Add ("Spell1", KeyCode.Alpha1);
		keys.Add ("Spell2", KeyCode.Alpha2);
		keys.Add ("Spell3", KeyCode.Alpha3);
		keys.Add ("Use", KeyCode.F);
        keys.Add("Potion HP", KeyCode.Alpha4);
        keys.Add("Potion MP", KeyCode.Alpha5);
    }

    void Update () {

        //animacja gracza
		playerAnim = GetComponent<Animator>();
        //klawisze w dół
//		Vector3 mousePos = Input.mousePosition;
//		Vector3 objectPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
//		mousePos.x = mousePos.x - objectPos.x;
//		mousePos.y = mousePos.y - objectPos.y;
//		float angle = Mathf.Atan2 (mousePos.y,mousePos.x)*Mathf.Rad2Deg;
//		gameObject.transform.rotation = Quaternion.Euler(new Vector3(0,0,angle+90));
        if (Input.GetKeyDown(keys["Right"]))
        {
            if(!right)
                right = true;
            playerAnim.SetBool("PlayerWalkRight", true);

        }

        if (Input.GetKeyDown(keys["Left"]))
        {
            if(!left)
                left = true;
            playerAnim.SetBool("PlayerWalkLeft", true);

        }

        if (Input.GetKeyDown(keys["Up"]))
        {
            if (!up)
                up = true;
            playerAnim.SetBool("PlayerWalkUp", true);
        }
        if (Input.GetKeyDown(keys["Down"]))
        {
            if (!down)
                down = true;
            playerAnim.SetBool("PlayerWalkDown", true);
        }

        //klawisze do góry
        if(Input.GetKeyUp(keys["Right"]))
        {
            if (right)
                right = false;
            playerAnim.SetBool("PlayerWalkRight", false);
        }
        if (Input.GetKeyUp(keys["Left"]))
        {
            if (left)
                left = false;
            playerAnim.SetBool("PlayerWalkLeft", false);
        }
        if (Input.GetKeyUp(keys["Up"]))
        {
            if (up)
                up = false;
            playerAnim.SetBool("PlayerWalkUp", false);
        }
        if (Input.GetKeyUp(keys["Down"]))
        {
            if (down)
                down = false;
            playerAnim.SetBool("PlayerWalkDown", false);
        }


        if (right)
                x = speed;
        if (left)
                x = -speed;
        if (up)
                y = speed;
        if (down)
                y = -speed;
        if (!right && !left)
            x = 0;
        if (!up && !down)
            y = 0;
        if (right && left)
            x = 0;
        if (up && down)
            y = 0;
		if (playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("attack_left") 
		    || playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("attack_right") 
		    || playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("attack_up")
			|| playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("attack_down")
			|| playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("cast_up") 
			|| playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("cast_down") 
			|| playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("cast_left") 
			|| playerAnim.GetCurrentAnimatorStateInfo (0).IsName ("cast_right") ) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
		} else {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (x, y);
		}
    }

}
