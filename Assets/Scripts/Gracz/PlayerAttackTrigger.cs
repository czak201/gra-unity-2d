﻿using UnityEngine;
using System.Collections;

public class PlayerAttackTrigger : MonoBehaviour {
	Collider2D attackTrigger;
	int PlayerDamage;
	PlayerManager parentclass;

	void Start()
	{
		parentclass = transform.parent.GetComponent<PlayerManager>();
		PlayerDamage = parentclass.PlayerDamage;
		attackTrigger = GetComponent<Collider2D>();
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.isTrigger != true && other.CompareTag("Enemy"))
        {
            other.SendMessageUpwards("ObrazeniaDlaZombika", PlayerDamage);
        }
    }
}
