﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour {

	private Image fillBar;
	// Use this for initialization
	void Start () {
		fillBar = this.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		fillBar.fillAmount = (float) PlayerManager.CurrentHealth/PlayerManager.MaxHealth;
	
	}
}
