﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ManaBarText : MonoBehaviour {

	private Text health;
	// Use this for initialization
	void Start () {
		health = this.GetComponent<Text>();
	}

	// Update is called once per frame
	void Update () {
		health.text = PlayerManager.CurrentMana.ToString() +"/"+ PlayerManager.MaxMana.ToString();
	}
}
