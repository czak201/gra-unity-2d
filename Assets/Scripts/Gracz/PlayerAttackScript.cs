﻿using UnityEngine;
using System.Collections;

public class PlayerAttackScript : MonoBehaviour {

    bool czyAtakowac;
    
    public Collider2D attackTriggerUp;
    public Collider2D attackTriggerDown;
    public Collider2D attackTriggerRight;
    public Collider2D attackTriggerLeft;
    public Animator animacja;

    void Awake()
    {
        //wylaczam na poczatku triggery w których zadajemy obrazenia
        animacja = GetComponent<Animator>();
        attackTriggerUp.enabled = false;
        attackTriggerDown.enabled = false;
        attackTriggerLeft.enabled = false;
        attackTriggerRight.enabled = false;
    }

    void Update()
    {
		if (animacja.GetBool ("PlayerCastUp"))
			animacja.SetBool ("PlayerCastUp", false);
		
		if(animacja.GetBool ("PlayerCastDown"))
			animacja.SetBool ("PlayerCastDown", false);

		if(animacja.GetBool ("PlayerCastLeft"))
			animacja.SetBool ("PlayerCastLeft", false);

		if(animacja.GetBool ("PlayerCastRight"))
			animacja.SetBool ("PlayerCastRight", false);

        //po wciśnięciu klawisza ataku (np. spacja) odpalają sie triggery i animacja ataku zgodnie z animacją patrzenia w jakimś kierunku
		if (Input.GetKey (Poruszanie.keys["Attack"])){

			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyUp") 
				|| animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkUp")) {
				animacja.SetBool ("PlayerAttackUp", true);
			}else
				animacja.SetBool ("PlayerAttackUp", false);
				

			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyDown") 
				|| animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkDown")) {
				animacja.SetBool ("PlayerAttackDown", true);
			}else
				animacja.SetBool ("PlayerAttackDown", false);
				

			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyLeft") 
				|| animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkLeft")) {
				animacja.SetBool ("PlayerAttackLeft", true);
			}else
				animacja.SetBool ("PlayerAttackLeft", false);
				

			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyRight") 
				|| animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkRight")) {
				animacja.SetBool ("PlayerAttackRight", true);
			}else
				animacja.SetBool ("PlayerAttackRight", false);
				
			} 
		if (Input.GetKey (Poruszanie.keys["Spell1"])
			||Input.GetKey (Poruszanie.keys["Spell2"])
			||Input.GetKey (Poruszanie.keys["Spell3"])) {

			if(Input.GetKey (Poruszanie.keys["Spell1"]))
				PlayerManager.CurrentSpell=1;
			
			if(Input.GetKey (Poruszanie.keys["Spell2"]))
				PlayerManager.CurrentSpell=2;
			
			if(Input.GetKey (Poruszanie.keys["Spell3"]))
				PlayerManager.CurrentSpell=3;
			
			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyUp")
			    || animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkUp")) {
				animacja.SetBool ("PlayerCastUp", true);
			}
				
			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyDown")
			    || animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkDown")) {
				animacja.SetBool ("PlayerCastDown", true);
			}


			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyLeft")
			    || animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkLeft")) {
				animacja.SetBool ("PlayerCastLeft", true);
			}


			if (animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerPatrzyRight")
			    || animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerWalkRight")) {
				animacja.SetBool ("PlayerCastRight", true);
			}
		} 
			if (!animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerAttackUp") && !animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerAttackDown") &&
				!animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerAttackLeft") && !animacja.GetCurrentAnimatorStateInfo (0).IsName ("PlayerAttackRight")) {
				attackTriggerUp.enabled = false;
				attackTriggerDown.enabled = false;
				attackTriggerLeft.enabled = false;
				attackTriggerRight.enabled = false;
			}
		}
    }
