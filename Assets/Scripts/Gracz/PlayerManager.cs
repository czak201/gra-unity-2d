﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public int PlayerHealth;
    public int PlayerMana;
    public int PlayerDamage;
    public int PlayerHPS;
    public int PlayerMPS;
    public int HealthPotionReplenishAmount;
    public int ManaPotionReplenishAmount;

    public int spell1ManaCost;
    public int spell1EffectAmount;
    public int spell2ManaCost;
    public int spell2EffectAmount;
    public int spell3ManaCost;
    public int spell3EffectAmount;

    public static int MaxHealth;
    public static int HealthPerSecond;
    public static int CurrentHealth;
    public static int HPRA;

    public static int MaxMana;
    public static int ManaPerSecond;
    public static int CurrentMana;
    public static int MPRA;

    public static int CurrentSpell;

    public static int Spell1ManaCost;
    public static int Spell1EffectAmount;

    public static int Spell2ManaCost;
    public static int Spell2EffectAmount;

    public static int Spell3ManaCost;
    public static int Spell3EffectAmount;

    public Text HealthPotionText;
    public Text ManaPotionText;
    public int HealthPotion = 10;
    public int ManaPotion = 10;

    public void Start()
    {
        CurrentHealth = PlayerHealth;
        HealthPerSecond = PlayerHPS;
        MaxHealth = PlayerHealth;
        HPRA = HealthPotionReplenishAmount;

        MaxMana = PlayerMana;
        ManaPerSecond = PlayerMPS;
        CurrentMana = PlayerMana;
        MPRA = ManaPotionReplenishAmount;

        CurrentSpell = 0;

        Spell1ManaCost = spell1ManaCost;
        Spell1EffectAmount = spell1EffectAmount;

        Spell2ManaCost = spell2ManaCost;
        Spell2EffectAmount = spell2EffectAmount;

        Spell3ManaCost = spell3ManaCost;
        Spell3EffectAmount = spell3EffectAmount;

        StartCoroutine(Regeneration());

        HealthPotionText.text = HealthPotion.ToString();
        ManaPotionText.text = ManaPotion.ToString();
    }

    IEnumerator Regeneration()
    {
        while (true)
        {
            if (CurrentHealth + HealthPerSecond > MaxHealth)
                CurrentHealth = MaxHealth;
            else
                CurrentHealth += HealthPerSecond;
            if (CurrentMana + ManaPerSecond > MaxMana)
                CurrentMana = MaxMana;
            else
                CurrentMana += ManaPerSecond;
            yield return new WaitForSeconds(5);
        }
    }

    //	void OnGUI()
    //	{
    //		GUI.BeginGroup (new Rect (Screen.width/2,Screen.height-barHeight, 400, barHeight));
    //			GUI.Box (new Rect (0, 0, healthBarLenght, barHeight),emptyHealthBar);
    //			GUI.BeginGroup (new Rect (0, 0, currentHealth, barHeight));
    //				GUI.Box (new Rect (0, 0, currentHealth, barHeight), fullHealthBar);
    //			GUI.EndGroup ();
    //		GUI.EndGroup ();
    //	}	

    // Update is called once per frame

	public void AddPotions(int[] array)
	{
		int type = array[0];
		int amount = array[1];
		switch(type)
		{
		case 1:
			HealthPotion+=amount;
			HealthPotionText.text = HealthPotion.ToString();
			break;
		case 2:
			ManaPotion+=amount;
			ManaPotionText.text = ManaPotion.ToString();
			break;
		}
	}
    public void DamageDealtToPlayer(int damageAmount)
    {
        CurrentHealth -= damageAmount;
        if (CurrentHealth <= 0)
        {
            Destroy(gameObject);
            Application.LoadLevel(1);
        }
    }

    void Update()
    {

        if (Input.GetKeyDown(Poruszanie.keys["Potion HP"]))
        {
            if (HealthPotion > 0 && CurrentHealth < MaxHealth)
            {

                if (CurrentHealth + HPRA > MaxHealth)
                {
                    HealthPotion -= 1;
                    CurrentHealth = MaxHealth;
                    HealthPotionText.text = HealthPotion.ToString();
                }
                else
                {
                    CurrentHealth += HPRA;
                    HealthPotion -= 1;
                    HealthPotionText.text = HealthPotion.ToString();
                }
            }
        }


        if (Input.GetKeyDown(Poruszanie.keys["Potion MP"]))
        {
            if (ManaPotion > 0 && CurrentMana < MaxMana)
            {
                if (CurrentMana + MPRA > MaxMana)
                {
                    CurrentMana = MaxMana;
                    ManaPotion -= 1;
                    ManaPotionText.text = ManaPotion.ToString();
                }

                else
                {
                    CurrentMana += MPRA;
                    ManaPotion -= 1;
                    ManaPotionText.text = ManaPotion.ToString();
                }
            }
        }
    }
}


