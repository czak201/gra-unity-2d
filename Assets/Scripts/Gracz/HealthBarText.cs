﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class HealthBarText : MonoBehaviour {

	private Text health;
	// Use this for initialization
	void Start () {
		health = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		health.text = PlayerManager.CurrentHealth.ToString() +"/"+ PlayerManager.MaxHealth.ToString();
	}
}
