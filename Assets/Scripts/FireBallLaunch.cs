﻿using UnityEngine;
using System.Collections;

public class FireBallLaunch : MonoBehaviour
{
	public int Speed;
	public GameObject fireballPrefab;
	public GameObject healingPrefab;
	private Animator animator;
	private int x,y,r;
	void Start()
	{
		animator = gameObject.GetComponent<Animator> ();

	}
	public void Shoot()
	{
		if (PlayerManager.CurrentSpell == 1) {
			if (PlayerManager.CurrentMana - PlayerManager.Spell1ManaCost >= 0) {
				x = 0;
				y = 0;
				r = 0;
				Vector2 pozycja = gameObject.GetComponentInParent<Rigidbody2D> ().transform.position;

				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_right")) {
					pozycja.x += 1;
					x = Speed;
					r = 0;
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_left")) {
					pozycja.x -= 2;
					x = -Speed;
					r = 180;
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_up")) {
					pozycja.y += 2;
					y = Speed;
					r = 90;
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_down")) {
					pozycja.y -= 2;
					y = -Speed;
					r = 270;
				}
				GameObject goneArrow = (GameObject)Instantiate (fireballPrefab, pozycja, Quaternion.Euler (0, 0, r));
				goneArrow.GetComponent<Rigidbody2D> ().velocity = new Vector2 (x, y);
				PlayerManager.CurrentMana -= PlayerManager.Spell1ManaCost;
			}
		}
		if (PlayerManager.CurrentSpell == 2) {
			if (PlayerManager.CurrentMana - PlayerManager.Spell2ManaCost >= 0) {
				PlayerManager.CurrentMana -= PlayerManager.Spell2ManaCost;

				Vector2 pozycja = gameObject.GetComponentInParent<Rigidbody2D> ().transform.position;

				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_right")) {
					pozycja.x += 6;
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_left")) {
					pozycja.x -= 6;
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_up")) {
					pozycja.y += 6;
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_down")) {
					pozycja.y -= 6;
				}
				Instantiate (healingPrefab, pozycja, Quaternion.Euler (0, 0, 0));
			}
		}
		if (PlayerManager.CurrentSpell == 3) {
			if (PlayerManager.CurrentMana - PlayerManager.Spell3ManaCost >= 0) {
				PlayerManager.CurrentMana -= PlayerManager.Spell3ManaCost;

				Vector2 pozycja = gameObject.GetComponentInParent<Rigidbody2D> ().transform.position;
				

				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_right")) {
					pozycja.x += 6;
					
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_left")) {
					pozycja.x -= 6;
					
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_up")) {
					pozycja.y += 6;
					
				}
				if (animator.GetCurrentAnimatorStateInfo (0).IsName ("cast_down")) {
					pozycja.y -= 6;
					
				}
			}
		}
	}
}
