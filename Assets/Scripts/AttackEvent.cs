﻿using UnityEngine;
using System.Collections;

public class AttackEvent : MonoBehaviour {
	public Collider2D attackTriggerUp;
	public Collider2D attackTriggerDown;
	public Collider2D attackTriggerLeft;
	public Collider2D attackTriggerRight;

	public void StartTrigger()
	{
		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_up"))
			attackTriggerUp.enabled = true;

		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_down"))
			attackTriggerDown.enabled = true;

		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_left"))
			attackTriggerLeft.enabled = true;

		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_right"))
			attackTriggerRight.enabled = true;
	}
	public void EndTrigger()
	{
		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_up"))
			attackTriggerUp.enabled = false;
		
		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_down"))
			attackTriggerDown.enabled = false;
		
		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_left"))
			attackTriggerLeft.enabled = false;
		
		if (gameObject.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("attack_right"))
			attackTriggerRight.enabled = false;
	}
}
