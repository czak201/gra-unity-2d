﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SkillManaRequirement : MonoBehaviour {
	private Image skillIcon;
	private Image[] images;
	// Use this for initialization
	void Start () {
		images = this.GetComponentsInChildren<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(PlayerManager.CurrentMana-PlayerManager.Spell1ManaCost<0){
			Color temp = images[1].color;
			temp.a=0.5f;
			images[1].color = temp;
		}else
			if(PlayerManager.CurrentMana-PlayerManager.Spell1ManaCost>=0){
				Color temp = images[1].color;
				temp.a=1f;
				images[1].color = temp;
			}
		if(PlayerManager.CurrentMana-PlayerManager.Spell2ManaCost<0){
			Color temp = images[2].color;
			temp.a=0.5f;
			images[2].color = temp;
		}else
			if(PlayerManager.CurrentMana-PlayerManager.Spell2ManaCost>=0){
				Color temp = images[2].color;
				temp.a=1f;
				images[2].color = temp;
			}
		if(PlayerManager.CurrentMana-PlayerManager.Spell3ManaCost<0){
			Color temp = images[3].color;
			temp.a=0.5f;
			images[3].color = temp;
		}else
			if(PlayerManager.CurrentMana-PlayerManager.Spell3ManaCost>=0){
				Color temp = images[3].color;
				temp.a=1f;
				images[3].color = temp;
			}
	}
}
