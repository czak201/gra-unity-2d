﻿using UnityEngine;
using System.Collections;

public class RandomNPCSpawn : MonoBehaviour {
	public GameObject[] NPCsToRandomSpawn;
	public int IntervalInSeconds;
	void Start()
	{
		StartCoroutine(Respawn());
	}
	IEnumerator Respawn()
	{
		while(true)
		{
			Vector2 NPCPosition = gameObject.transform.position;
			Instantiate(NPCsToRandomSpawn[Random.Range(0, NPCsToRandomSpawn.Length)], NPCPosition, Quaternion.identity);
			yield return new WaitForSeconds(IntervalInSeconds);
		}
	}

}
