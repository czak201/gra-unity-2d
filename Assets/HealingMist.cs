﻿using UnityEngine;
using System.Collections;

public class HealingMist : MonoBehaviour {
	public int HealAmount;

	// Use this for initialization

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "Player") {
			if (PlayerManager.CurrentHealth + HealAmount > PlayerManager.MaxHealth)
				PlayerManager.CurrentHealth = PlayerManager.MaxHealth;
			else
				PlayerManager.CurrentHealth += HealAmount;
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
