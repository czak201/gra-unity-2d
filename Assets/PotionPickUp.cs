﻿using UnityEngine;
using System.Collections;

public class PotionPickUp : MonoBehaviour {
	public int typeOfPotions;
	public int amountOfPotions;
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.isTrigger != true && other.CompareTag("Player"))
		{
			int[] newObject = new int[2];
			newObject[0]=typeOfPotions;
			newObject[1]=amountOfPotions;
			other.SendMessageUpwards("AddPotions", newObject);
			Destroy(gameObject);
		}
	}
}
